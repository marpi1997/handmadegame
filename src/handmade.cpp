/*
$File: $safeitemname$
$Date: 02/05/2015 $time$
$Revision: $
$Author: Markus Pinter $username$
$Version: $clrversion$
$Notice: $
*/

#include "handmade.h"
#include <math.h>

internal void GameOutputSound(game_sound_output_buffer *soundBuffer, int toneHz)
{
	local_persist real32 tSine;
	int16 toneVolume = 3000;
	int wavePeriod = soundBuffer->samplesPerSecond / toneHz;

	int16 *sampleOut = soundBuffer->samples;
	for (int sampleIndex = 0; sampleIndex < soundBuffer->sampleCount; sampleIndex++)
	{
		real32 sineValue = sinf(tSine);
		int16 sampleValue = (int16)(sineValue * toneVolume);

		*sampleOut++ = sampleValue;
		*sampleOut++ = sampleValue;

		tSine += 2.0f * PI32 * 1.0f / (real32)wavePeriod;
	}
}

internal void RenderWeirdGradient(game_offscreen_buffer *buffer, int xOffset, int yOffset)
{
	uint8 *row = (uint8 *)buffer->memory;

	uint32 *pixel;

	uint8 blue;
	uint8 green;
	uint8 red;
	uint8 padding;

	for (int y = 0; y < buffer->height; y++)
	{
		pixel = (uint32 *)row;
		for (int x = 0; x < buffer->width; x++)
		{
			/*
			Pixel in memory: BB GG RR xx
			LITTLE ENDIAN ARCHITECTURE
			*/
			blue = (x + xOffset);
			green = (y + yOffset);
			red = xOffset + yOffset;
			padding = 0;

			*pixel++ = ((padding << 24) | (red << 16) | (green << 8) | blue);
		}
		row += buffer->pitch;
	}
}

internal void GameUpdateAndRender(game_input *input, game_offscreen_buffer *buffer,
								  game_sound_output_buffer *soundBuffer)
{
	//	TODO: Allow sample offsets here for more robust platform options
	int const c1 = 261;
	local_persist int toneHz = c1;
	local_persist int xOffset = 0;
	local_persist int yOffset = 0;

	//	TODO: pull out offsets and tone and make it static and clean up in win32
	//	TODO: check if input comes from an gampad or keyboard

	//	TODO: implement input handling without event storing
	//	TODO: as well as tone and offset updating inflicted by the gamepad stick
	//	TODO: note multiplayer support
	//	TODO: implement cap for controllerIndex
	//	TODO: normalize stick values

	for (int inputIndex = 0; inputIndex < ArrayCount(input->controllers); inputIndex++)
	{
		game_controller_input *currInput = &input->controllers[inputIndex];
		if (currInput->isAnalog)
		{
			if (&currInput->rightShoulder)
			{
				toneHz = c1 + (int)(128.0f * (currInput->endX));
				xOffset += (int)(4.0f * (currInput->endY));
			}
			else
			{
				if (toneHz > 440)
				{
					toneHz--;
				}
				else if (toneHz < 440)
				{
					toneHz++;
				}
			}

			/*if (currInput->downButton)
			{
				soundOutput->toneHz -= 20;
				char msgbuffer[64];
				sprintf(msgbuffer, "Tone: %d\n", soundOutput->toneHz);
				OutputDebugString(msgbuffer);
			}
			if (xButton)
			{
				soundOutput->toneHz--;
				char msgbuffer[64];
				sprintf(msgbuffer, "Tone: %d\n", soundOutput->toneHz);
				OutputDebugString(msgbuffer);
			}
			if (yButton)
			{
				soundOutput->toneHz++;
				char msgbuffer[64];
				sprintf(msgbuffer, "Tone: %d\n", soundOutput->toneHz);
				OutputDebugString(msgbuffer);
			}
			if (bButton)
			{
				soundOutput->toneHz += 20;
				char msgbuffer[64];
				sprintf(msgbuffer, "Tone: %d\n", soundOutput->toneHz);
				OutputDebugString(msgbuffer);
			}*/
		}
		else
		{
			//NOTE: digital
		}
		
		if (currInput->downButton.isButtonEndPositionDown)
		{
			yOffset += 1;
		}
	}

	GameOutputSound(soundBuffer, toneHz);
	RenderWeirdGradient(buffer, xOffset, yOffset);
}