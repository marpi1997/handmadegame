#ifndef HANDMADE_H

/*
$File: $safeitemname$
$Date: 02/05/2015 $time$
$Revision: $
$Author: Markus Pinter $username$
$Version: $clrversion$
$Notice: $
*/


#include "typedefs.h"

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))
/*
TODO: Services that the platform layer provides to the game
*/

/*
NOTE: Services that the game provides to the platform layer
(this may expand in the future - sound on separate thread, etc.)
*/

struct game_sound_output_buffer
{
	int samplesPerSecond;
	int sampleCount;
	int16 *samples;
};

// FOUR THINGS - timing, controller/keyboard input, bitmap buffer to use, sound buffer to use
struct game_offscreen_buffer
{
	// NOTE: Pixels are always 32-bits wide, Memory Order BB GG RR XX
	void *memory;
	int width;
	int height;
	int pitch;
};

struct game_button_state
{
	int halfTransitionCount;
	bool32 isButtonEndPositionDown;
};

struct game_controller_input
{
	bool32 isAnalog;

	real32 startX;
	real32 startY;

	real32 minX;
	real32 minY;

	real32 maxX;
	real32 maxY;

	real32 endX;
	real32 endY;

	union
	{
		game_button_state buttons[8];
		struct 
		{
			game_button_state upButton;
			game_button_state downButton;
			game_button_state leftButton;
			game_button_state rightButton;
			game_button_state leftShoulder;
			game_button_state rightShoulder;
			game_button_state startButton;
			game_button_state selectButton;
		};
	};
};

struct game_input
{
	game_controller_input controllers[4];
};

internal void GameUpdateAndRender(game_input *input, game_offscreen_buffer *buffer,
								  game_sound_output_buffer *soundBuffer);


#define HANDMADE_H
#endif