@echo off

pushd W:\C++\HandmadeGame

IF NOT EXIST bin mkdir bin
pushd bin

cl -FC -Zi ..\src\win32_handmade.cpp User32.lib Gdi32.lib

popd
popd
