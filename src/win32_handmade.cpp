/*
$File: $safeitemname$
$Date: 31/03/2015$time$
$Revision: $
$Author: Markus Pinter $
$Version: $clrversion$
$Notice: $
*/

/*
	THIS IS NOT A FINAL PLATFORM LAYER!!!

	* Saved game locations
	* Getting a ahandle to our own executable file
	* Threading (launch a thread)
	* Raw Input (support for multiple keyboards
	* Sleep/timeBeginPeriod
	* ClipCursor() (for multimonitor support)
	* Fullscreen support
	* WM_SETCURSOR (control cursor visibility)
	* QueryCancelAutoplay
	* WM_ACTIVATEAPP (for when we are not the active application)
	* Blit speed improvements (BitBlt)
	* Hardware acceleration (OpenGL or Direct3D or BOTH??)
	* GetKeyboardLayout (for French keyboards, international WASD support)

	Just a partial list of stuff!!
*/

#include <iostream>
#include "typedefs.h"

#include "handmade.cpp"

#include <windows.h>
#include <Xinput.h>
#include <dsound.h>

struct win32OffscreenBuffer 
{
	BITMAPINFO info;
	void *memory;
	int width;
	int height;
	int pitch;
	int bytesPerPixel;
};

struct win32WindowDimension 
{
	int width;
	int height;
};

// will be changed later 
global_variable bool32 running;
global_variable win32OffscreenBuffer globalBuffer;
global_variable LPDIRECTSOUNDBUFFER globalSecondarySoundBuffer;

/*------------------------------------------------------------------
SOUND STUFF
------------------------------------------------------------------*/

struct win32SoundOutput
{
	int samplesPerSecond;
	uint32 runningSampleIndex;
	int bytesPerSample;
	int secondaryBufferSize;
	real32 tSine;
	int latencySampleCount;
};

#define DIRECT_SOUND_CREATE(name) HRESULT WINAPI name(LPCGUID pcGuidDevice, LPDIRECTSOUND *ppDS, LPUNKNOWN pUnkOuter)
typedef DIRECT_SOUND_CREATE(direct_sound_create);

global_variable direct_sound_create  *directSoundCreate;
#define DirectSoundCreate directSoundCreate

internal void initDSound(HWND window, int32 bufferSize, int32 soundChannels, int32 samplesPerSecond, int32 bitsPerSample)
{
	//	load the library
	HMODULE dSoundLibrary = LoadLibraryA("dsound.dll");

	if (dSoundLibrary)
	{
		//	get a DirectSound object! - cooperative
		direct_sound_create *directSoundCreate =
			(direct_sound_create *)GetProcAddress(dSoundLibrary, "DirectSoundCreate");

		//	TODO: Double-check that this works on XP - DirectSound8 or 7??
		LPDIRECTSOUND directSound;
		if (directSoundCreate && SUCCEEDED(DirectSoundCreate(0, &directSound, 0)))
		{

			WAVEFORMATEX waveFormat = {};
			waveFormat.wFormatTag = WAVE_FORMAT_PCM;
			waveFormat.nChannels = soundChannels;
			waveFormat.nSamplesPerSec = samplesPerSecond;
			waveFormat.wBitsPerSample = bitsPerSample;
			waveFormat.nBlockAlign = (waveFormat.nChannels * waveFormat.wBitsPerSample) / 8;
			waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
			waveFormat.cbSize = 0;

			if (SUCCEEDED(directSound->SetCooperativeLevel(window, DSSCL_PRIORITY)))
			{
				//	"Create" a primary buffer --> handle to the sound card to set the waveFormat
				//	TODO: DSBCAPS_GLOBALFOCUS?
				DSBUFFERDESC bufferDescription = {};
				bufferDescription.dwSize = sizeof(bufferDescription);
				bufferDescription.dwFlags = DSBCAPS_PRIMARYBUFFER;
				bufferDescription.dwBufferBytes = 0;

				LPDIRECTSOUNDBUFFER primaryBuffer;
				if (SUCCEEDED(directSound->CreateSoundBuffer(&bufferDescription, &primaryBuffer, 0)))
				{
					HRESULT error = primaryBuffer->SetFormat(&waveFormat);
					if (SUCCEEDED(error))
					{
						//	we have finally set the format!
						OutputDebugStringA("Primary buffer format was sent.\n");
					}
					else
					{
						//	TODO: diagnostic
					}

				}
				else
				{
					//	TODO: diagnostic
				}

			}
			else
			{
				//	TODO: diagnostic
			}
			//	"Create" a secondary buffer
			//	TODO: DSBCAPS_GETCURRENTPOSITION2
			DSBUFFERDESC bufferDescription = {};
			bufferDescription.dwSize = sizeof(bufferDescription);
			bufferDescription.dwFlags = 0;
			bufferDescription.dwBufferBytes = bufferSize;
			bufferDescription.lpwfxFormat = &waveFormat;

			if (SUCCEEDED(directSound->CreateSoundBuffer(&bufferDescription, &globalSecondarySoundBuffer, 0)))
			{
				//	start it playing!
				OutputDebugStringA("Secondary buffer created successfully.\n");
			}
			else
			{
				//	TODO: diagnostic
			}
		}
		else
		{
			//	TODO: diagnostic
		}
	}
	else
	{
		//	TODO: diagnostic
	}


}

internal void clearBuffer(win32SoundOutput *soundOutput)
{
	VOID *region1;
	DWORD region1Size;
	VOID *region2;
	DWORD region2Size;

	if (SUCCEEDED(globalSecondarySoundBuffer->Lock(0, soundOutput->secondaryBufferSize,
		&region1, &region1Size,
		&region2, &region2Size,
		0)))
	{
		uint8 *destSample = (uint8*)region1;
		for (DWORD sampleIndex = 0; sampleIndex < region1Size; sampleIndex++)
		{
			*destSample++ = 0;
		}

		destSample = (uint8*)region2;
		for (DWORD sampleIndex = 0; sampleIndex < region2Size; sampleIndex++)
		{
			*destSample++ = 0;
		}


		globalSecondarySoundBuffer->Unlock(region1, region1Size, region2, region2Size);
	}
}

internal void fillSoundBuffer(win32SoundOutput *soundOutput,
	DWORD byteToLock,
	DWORD bytesToWrite,
	game_sound_output_buffer *sourceBuffer)
{
	//	int16	int16	int16	...					<- BitsPerSample
	//	[LEFT	RIGHT]	LEFT	RIGHT	LEFT	... <- Channels
	//	single Sample

	VOID *region1;
	DWORD region1Size;
	VOID *region2;
	DWORD region2Size;

	if (SUCCEEDED(globalSecondarySoundBuffer->Lock(byteToLock, bytesToWrite, &region1, &region1Size, &region2, &region2Size, 0)))
	{
		int16 *destSample = (int16*)region1;
		int16 *sourceSample = sourceBuffer->samples;
		DWORD region1SampleCount = region1Size / soundOutput->bytesPerSample;

		for (DWORD sampleIndex = 0; sampleIndex < region1SampleCount; sampleIndex++)
		{
			*destSample++ = *sourceSample++;
			*destSample++ = *sourceSample++;
			++soundOutput->runningSampleIndex;
		}

		destSample = (int16*)region2;
		DWORD region2SampleCount = region2Size / soundOutput->bytesPerSample;

		for (DWORD sampleIndex = 0; sampleIndex < region2SampleCount; sampleIndex++)
		{
			*destSample++ = *sourceSample++;
			*destSample++ = *sourceSample++;
			++soundOutput->runningSampleIndex;
		}

		globalSecondarySoundBuffer->Unlock(region1, region1Size, region2, region2Size);
	}
}

/*------------------------------------------------------------------
END OF SOUND STUFF
------------------------------------------------------------------*/


/*------------------------------------------------------------------
						CONTROLLER STUFF
------------------------------------------------------------------*/

#define X_INPUT_GET_STATE(name) DWORD WINAPI name(DWORD dwUserIndex, XINPUT_STATE *pState)
typedef X_INPUT_GET_STATE(x_input_get_state);

X_INPUT_GET_STATE(xInputGetStateStub)
{
	return ERROR_DEVICE_NOT_CONNECTED;
}
global_variable x_input_get_state *xInputGetState = xInputGetStateStub;
#define XInputGetState xInputGetState 

#define X_INPUT_SET_STATE(name) DWORD WINAPI name(DWORD dwUserIndex, XINPUT_VIBRATION *pVibration)
typedef X_INPUT_SET_STATE(x_input_set_state);

X_INPUT_SET_STATE(xInputSetStateStub)
{
	return ERROR_DEVICE_NOT_CONNECTED;
}
global_variable x_input_set_state *xInputSetState = xInputSetStateStub;
#define XInputSetState xInputSetState 

internal void loadXInput()
{
	// Test this on Windows 8
	HMODULE xInputLibrary = LoadLibraryA("xinput1_4.dll");

	if (!xInputLibrary)
	{
		//	TODO: diagnostic
		xInputLibrary = LoadLibraryA("xinput1_3.dll");
	}

	if (!xInputLibrary)
	{
		//	TODO: diagnostic
		xInputLibrary = LoadLibraryA("xinput9_1_0.dll");
	}

	if (xInputLibrary)
	{
		XInputGetState = (x_input_get_state *)GetProcAddress(xInputLibrary, "XInputGetState");
		XInputSetState = (x_input_set_state *)GetProcAddress(xInputLibrary, "XInputSetState");
		//	TODO: diagnostic

	}
	else
	{
		//	TODO: diagnostic
	}
}

internal void processXInputDigitalButton(game_button_state *oldState, game_button_state *newState, DWORD xInputButtonState, DWORD buttonBit)
{
	oldState->isButtonEndPositionDown = ((xInputButtonState & buttonBit) == buttonBit);
	oldState->halfTransitionCount = 
		(oldState->isButtonEndPositionDown != newState->isButtonEndPositionDown) ? 1 : 0;


}

internal void pollController(game_input *input, win32SoundOutput *soundOutput)
{
	XINPUT_VIBRATION vibration;

	game_input *newInput = input;
	game_input *oldInput = {};

	int maxControllerCount = XUSER_MAX_COUNT;

	if (maxControllerCount > ArrayCount(&newInput->controllers))
	{
		maxControllerCount = ArrayCount(&newInput->controllers);
	}

	for (DWORD controllerIndex = 0; controllerIndex < maxControllerCount; controllerIndex++)
	{
		game_controller_input *oldController = &oldInput->controllers[controllerIndex];
		game_controller_input *newController = &newInput->controllers[controllerIndex];

		XINPUT_STATE controllerState;
		if (XInputGetState(controllerIndex, &controllerState) == ERROR_SUCCESS)
		{
			//	This controller is plugged in
			//	TODO: See if ControllerState.dwPacketNumber increment
			XINPUT_GAMEPAD *gamePad = &controllerState.Gamepad;
			
			bool32 up = (gamePad->wButtons & XINPUT_GAMEPAD_DPAD_UP);
			bool32 down = (gamePad->wButtons & XINPUT_GAMEPAD_DPAD_DOWN);
			bool32 left = (gamePad->wButtons & XINPUT_GAMEPAD_DPAD_LEFT);
			bool32 right = (gamePad->wButtons & XINPUT_GAMEPAD_DPAD_RIGHT);

			int16 stickX = gamePad->sThumbLX;
			int16 stickY = gamePad->sThumbLY;

			processXInputDigitalButton(
				&oldController->downButton, &newController->downButton,
				gamePad->wButtons, XINPUT_GAMEPAD_A);

			processXInputDigitalButton(
				&oldController->rightButton, &newController->rightButton,
				gamePad->wButtons, XINPUT_GAMEPAD_B);

			processXInputDigitalButton(
				&oldController->leftButton, &newController->leftButton,
				gamePad->wButtons, XINPUT_GAMEPAD_X);

			processXInputDigitalButton(
				&oldController->upButton, &newController->upButton,
				gamePad->wButtons, XINPUT_GAMEPAD_Y);

			processXInputDigitalButton(
				&oldController->leftShoulder, &newController->leftShoulder,
				gamePad->wButtons, XINPUT_GAMEPAD_LEFT_SHOULDER);

			processXInputDigitalButton(
				&oldController->rightShoulder, &newController->rightShoulder,
				gamePad->wButtons, XINPUT_GAMEPAD_RIGHT_SHOULDER);

			processXInputDigitalButton(
				&oldController->startButton, &newController->startButton,
				gamePad->wButtons, XINPUT_GAMEPAD_START);

			processXInputDigitalButton(
				&oldController->selectButton, &newController->selectButton,
				gamePad->wButtons, XINPUT_GAMEPAD_BACK);


			bool32 start = (gamePad->wButtons & XINPUT_GAMEPAD_START);
			bool32 back = (gamePad->wButtons & XINPUT_GAMEPAD_BACK);
			bool32 leftShoulder = (gamePad->wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER);
			bool32 rightShoulder = (gamePad->wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER);

			bool32 aButton = (gamePad->wButtons & XINPUT_GAMEPAD_A);
			bool32 bButton = (gamePad->wButtons & XINPUT_GAMEPAD_B);
			bool32 xButton = (gamePad->wButtons & XINPUT_GAMEPAD_X);
			bool32 yButton = (gamePad->wButtons & XINPUT_GAMEPAD_Y);

			

			if (start)
			{
				vibration.wLeftMotorSpeed = 5000;
				vibration.wRightMotorSpeed = 5000;

				XInputSetState(0, &vibration);
			}
			if (back)
			{
				vibration.wLeftMotorSpeed = 0;
				vibration.wRightMotorSpeed = 0;

				XInputSetState(0, &vibration);
			}

			oldInput = newInput;

		}
		else
		{
			//	This controller is not available
		}
	}
}

/*------------------------------------------------------------------
					END OF CONTROLLER STUFF
------------------------------------------------------------------*/


internal win32WindowDimension getWindowDimension(HWND window)
{
	win32WindowDimension result;

	RECT clientRect;
	GetClientRect(window, &clientRect);
	result.width = clientRect.right - clientRect.left;
	result.height = clientRect.bottom - clientRect.top;

	return result;
}

internal void resizeDIBSection(win32OffscreenBuffer *buffer, int width, int height)
{
	// bulletproof this-
	// maybe don't free first, free after, then free first if that fails.

	if (buffer->memory) 
	{
		VirtualFree(buffer->memory, 0, MEM_RELEASE);
	}

	buffer->width = width;
	buffer->height = height;
	buffer->bytesPerPixel = 4;

	buffer->info.bmiHeader.biSize = sizeof(buffer->info.bmiHeader);
	buffer->info.bmiHeader.biWidth = width;
	buffer->info.bmiHeader.biHeight = -height;
	buffer->info.bmiHeader.biPlanes = 1;
	buffer->info.bmiHeader.biBitCount = 32;
	buffer->info.bmiHeader.biCompression = BI_RGB;

	// Thank you to Chris Hecker of Spy Party fame
	// for clarifying the deal with StretchDIBits and BitBlt!
	// No more DC for us.

	int bitmapMemorySize = (width * height) * buffer->bytesPerPixel;
	buffer->memory = VirtualAlloc(0, bitmapMemorySize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);

	buffer->pitch = width * buffer->bytesPerPixel;

}

internal void loadBuffer(win32OffscreenBuffer *buffer, HDC deviceContext, int windowWidth, int windowHeight, int x, int y, int width, int height)
{
	// Aspect ratio correction
	//	play with stretch modes
	StretchDIBits(deviceContext, 
			    /*x, y, width, height, 
				  x, y, width, height,*/
				  0, 0, windowWidth, windowHeight,
				  0, 0, buffer->width, buffer->height,
				  buffer->memory,
				  &buffer->info,
				  DIB_RGB_COLORS,
				  SRCCOPY);
}

LRESULT CALLBACK mainWindowCallback(
	HWND window,
	UINT message,
	WPARAM wParam,
	LPARAM lParam)
{
	LRESULT result = 0;

	if (message == WM_SIZE) 
	{
		
		OutputDebugStringA("WM_SIZE\n");
	}
	else if (message == WM_DESTROY) 
	{
		OutputDebugStringA("WM_DESTROY\n");
		running = false;
	}
	else if (message == WM_CLOSE)
	{
		OutputDebugStringA("WM_CLOSE\n");
		running = false;
	}

	else if (message == WM_SYSKEYUP || message == WM_SYSKEYDOWN || 
		     message == WM_KEYUP || message == WM_KEYDOWN)
	{
		uint32 vKeyCode = wParam;

		bool32 altKeyWasDown = lParam & (1 << 29);
		bool32 wasDown = lParam & (1 << 30); // 30 is the code of lparam if key was down
		bool32 isDown = ((lParam & (1 << 31)) == 0);
		
		if (wasDown != isDown)
		{
			if (vKeyCode == 'W')
			{
				OutputDebugStringA("W: ");
				if (isDown)
				{
					OutputDebugStringA("isDown ");
				}
				if (wasDown)
				{
					OutputDebugStringA("wasDown ");
				}
				OutputDebugStringA("\n");
			}
			else if (vKeyCode == 'A')
			{

			}
			else if (vKeyCode == 'S')
			{

			}
			else if (vKeyCode == 'D')
			{

			}
			else if (vKeyCode == 'Q')
			{

			}
			else if (vKeyCode == 'E')
			{

			}
			else if (vKeyCode == VK_UP)
			{

			}
			else if (vKeyCode == VK_DOWN)
			{

			}
			else if (vKeyCode == VK_LEFT)
			{

			}
			else if (vKeyCode == VK_RIGHT)
			{

			}
			else if (vKeyCode == VK_ESCAPE)
			{
				running = false;
			}
			else if (vKeyCode == VK_SPACE)
			{

			}
		}

		if ((vKeyCode == VK_F4) && altKeyWasDown)
		{
			running = false;
		}

	}
	else if (message == WM_ACTIVATEAPP) 
	{
		OutputDebugStringA("WM_ACTIVATEAPP\n");
	}
	else if (message == WM_CREATE) 
	{
		OutputDebugStringA("WM_CREATE\n");
	}
	else if (message == WM_PAINT) 
	{

		OutputDebugStringA("painting\n");

		PAINTSTRUCT graphics;
		HDC deviceContext = BeginPaint(window, &graphics);

		int x = graphics.rcPaint.left;
		int y = graphics.rcPaint.top;

		int width = graphics.rcPaint.right - x;
		int height = graphics.rcPaint.bottom - y;

		win32WindowDimension dimension = getWindowDimension(window);
		loadBuffer(&globalBuffer, deviceContext, dimension.width, dimension.height, 0, 0, dimension.width, dimension.height);

		EndPaint(window, &graphics);

	}
	else 
	{
		//OutputDebugStringA("default\n");
		result = DefWindowProcA(window, message, wParam, lParam);
	}

	return result;
}


int CALLBACK WinMain(
	_In_  HINSTANCE hInstance,
	_In_  HINSTANCE hPrevInstance,
	_In_  LPSTR lpCmdLine,
	_In_  int nCmdShow) 
{

	LARGE_INTEGER perfCountFrequencyResult;
	QueryPerformanceFrequency(&perfCountFrequencyResult);
	int64 perfCountFrequency = perfCountFrequencyResult.QuadPart;

	loadXInput();

	WNDCLASS windowStruct = {};
	
	// TODO: Check if style-flags still matter: CS_OWNDC
	windowStruct.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	windowStruct.lpfnWndProc = mainWindowCallback;
	windowStruct.hInstance = hInstance;
	windowStruct.lpszClassName = "Handmade Game";
	
	if (RegisterClassA(&windowStruct))
	{
		HWND window = CreateWindowExA(
			0,
			windowStruct.lpszClassName,
			"Handmade Game",
			WS_OVERLAPPEDWINDOW | WS_VISIBLE,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			0,
			0,
			hInstance,
			0);

		if (window != NULL)
		{		
			MSG message;

			game_input input = {};
			
			win32SoundOutput soundOutput = {};

			// TODO: make this like sixty seconds?
			soundOutput.samplesPerSecond = 48000;
			soundOutput.runningSampleIndex = 0;
			soundOutput.bytesPerSample = sizeof(int16) * 2;
			soundOutput.secondaryBufferSize = soundOutput.samplesPerSecond * soundOutput.bytesPerSample;
			soundOutput.latencySampleCount = soundOutput.samplesPerSecond / 15;

			initDSound(window, soundOutput.secondaryBufferSize, 2, soundOutput.samplesPerSecond, 16);
			clearBuffer(&soundOutput);
			globalSecondarySoundBuffer->Play(0, 0, DSBPLAY_LOOPING);

			//	since we specified CD_OWNDC, we can just 
			//	get one device context and use it forever because we
			//	are not sharing it with anyone.
			HDC deviceContext = GetDC(window);

			resizeDIBSection(&globalBuffer, 1280, 720);

			LARGE_INTEGER lastCounter;
			QueryPerformanceCounter(&lastCounter);
			uint64 lastCycleCount = __rdtsc();

			running = true;

			// TODO: Pool with bitmap VirtualAlloc
			int16 *samples = (int16 *)VirtualAlloc(0, soundOutput.secondaryBufferSize, 
											MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

			while (running)
			{
				while (PeekMessageA(&message, 0, 0, 0, PM_REMOVE))
				{
					if (message.message == WM_QUIT)
					{
						running = false;
					}
					TranslateMessage(&message);
					DispatchMessageA(&message);
				}

				pollController(&input, &soundOutput);

				win32WindowDimension dimension = getWindowDimension(window);

				
				DWORD playCursor;
				DWORD writeCursor;

				DWORD byteToLock;
				DWORD targetCursor;
				DWORD bytesToWrite;

				bool32 soundIsValid = false;

				// TODO: Tighten up sound logic so that we know where we should be
				// writing to and can anticipate the time spent in the game update.
				if (SUCCEEDED(globalSecondarySoundBuffer->GetCurrentPosition(&playCursor, &writeCursor)))
				{
					byteToLock = (soundOutput.runningSampleIndex * soundOutput.bytesPerSample) 
									% soundOutput.secondaryBufferSize;

					targetCursor = (playCursor + (soundOutput.latencySampleCount * soundOutput.bytesPerSample)) 
									% soundOutput.secondaryBufferSize;
					// TODO: we need a more accurate check than byteToLock == playCursor

					if (byteToLock > targetCursor)
					{
						bytesToWrite = (soundOutput.secondaryBufferSize - byteToLock);
						bytesToWrite += targetCursor;
					}
					else
					{
						bytesToWrite = (targetCursor - byteToLock);
					}

					soundIsValid = true;
				}

				game_sound_output_buffer soundBuffer = {};
				soundBuffer.samplesPerSecond = soundOutput.samplesPerSecond;
				soundBuffer.sampleCount = bytesToWrite / soundOutput.bytesPerSample;
				soundBuffer.samples = samples;

				game_offscreen_buffer buffer = {};
				buffer.memory = globalBuffer.memory;
				buffer.width = globalBuffer.width;
				buffer.height = globalBuffer.height;
				buffer.pitch = globalBuffer.pitch;

				GameUpdateAndRender(&input, &buffer, &soundBuffer);

				if (soundIsValid)
				{
					fillSoundBuffer(&soundOutput, byteToLock, bytesToWrite, &soundBuffer);
				}

				loadBuffer(&globalBuffer, deviceContext, dimension.width, dimension.height, 0, 0, dimension.width, dimension.height);
				ReleaseDC(window, deviceContext);

				uint64 endCycleCount = __rdtsc();

				LARGE_INTEGER endCounter;
				QueryPerformanceCounter(&endCounter);

				//	TODO: Display the value here
				uint64 cyclesElapsed = endCycleCount - lastCycleCount;
				int64 counterElapsed = endCounter.QuadPart - lastCounter.QuadPart;
				real32 msPerFrame = ((1000.0f * counterElapsed) / perfCountFrequency);

				real32 fps = (real32)perfCountFrequency / counterElapsed;
				real32 megaCyclesPerFrame = cyclesElapsed / (1000.0f * 1000.0f);

				char msgbuffer[64];
				sprintf(msgbuffer, "%.2f ms/f, %.2f f/s, %.2f Mc/f\n", msPerFrame, fps, megaCyclesPerFrame);
				OutputDebugString(msgbuffer);

				lastCounter = endCounter;
				lastCycleCount = endCycleCount;
			}
			globalSecondarySoundBuffer->Stop();
		}
		else 
		{
			// TODO: Logging
		}
	}
	else 
	{
		// TODO: Logging
	}

	return 0;
}